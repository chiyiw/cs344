#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define BUFF_SIZE 256
#define MAX_SIZE 1024 * 1024

// Error function used for reporting issues
void error(const char *msg)
{
    perror(msg);
    exit(1);
}

// OTP decryption algorithm
void decrypt(char *cipher, char* key, char *plain, int len)
{
    int i = 0;
    char c[MAX_SIZE], k[MAX_SIZE], p[MAX_SIZE];
    for (i; i < len; i++)
    {
        if (cipher[i] == ' ') c[i] = 26;
        else c[i] = cipher[i] - 'A';
        if (key[i] == ' ') k[i] = 26;
        else k[i] = key[i] - 'A';

        p[i] = (c[i] + 27 - k[i]) % 27;

        if (p[i] == 26) plain[i] = ' ';
        else plain[i] = p[i] + 'A';
    }
}

int main(int argc, char *argv[])
{
    int listen_fd, conn_fd, port;
    socklen_t addr_len;
    char buffer[BUFF_SIZE];
    struct sockaddr_in server_addr, client_addr;

    char plain[MAX_SIZE], key[MAX_SIZE], cipher[MAX_SIZE];
    int cipher_len = 0, key_len = 0;

    if (argc < 2)
    {
        fprintf(stderr, "USAGE: %s port\n", argv[0]);
        exit(1);
    }

    // Set up the address struct for this process (the server)
    memset((char *)&server_addr, '\0', sizeof(server_addr)); // Clear out the address struct
    port = atoi(argv[1]);                                    // Get the port number, convert to an integer from a string
    server_addr.sin_family = AF_INET;                        // Create a network-capable socket
    server_addr.sin_port = htons(port);                      // Store the port number
    server_addr.sin_addr.s_addr = INADDR_ANY;                // Any address is allowed for connection to this process

    // Set up the socket
    listen_fd = socket(AF_INET, SOCK_STREAM, 0); // Create the socket
    if (listen_fd < 0)
        error("ERROR opening socket");

    // Enable the socket to begin listening
    if (bind(listen_fd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) // Connect socket to port
        error("ERROR on binding");
    listen(listen_fd, 5); // Flip the socket on - it can now receive up to 5 connections

    while (1)
    {
        // Accept a connection, blocking if one is not available until one connects
        addr_len = sizeof(client_addr);                                          // Get the size of the address for the client that will connect
        conn_fd = accept(listen_fd, (struct sockaddr *)&client_addr, &addr_len); // Accept
        if (conn_fd < 0) error("ERROR on accept");

        // fork to handle client connection
        int pid = fork();
        if (pid < 0) error("ERROR: otp_enc_d fork error");
        if (pid > 0) {
            if (waitpid(pid, NULL, 0) != pid) error("ERROR on waitpid");
            continue;
        };

        // fork twice to avoid zombie process
        int pid2 = fork();
        if (pid2 < 0) error("ERROR: otp_enc_d fork error");
        if (pid2 > 0) exit(0);

        // Receive header
        int read_len = recv(conn_fd, buffer, 12, 0);
        if (read_len < 0) error("ERROR reading from socket");
        // Check whether from otp_dec
        if (strncmp(buffer, "decr", 4) != 0) {
            if (send(conn_fd, "1111", 4, 0) < 0)
                error("ERROR writting to socket");
            sleep(3);
            exit(0);
        }
        cipher_len = ntohl(*((int*)buffer + 1));
        key_len = ntohl(*((int*)buffer + 2));

        // Receive ciphertext and key
        int index = 0;
        while (index < cipher_len + key_len)
        {
            read_len = recv(conn_fd, buffer, BUFF_SIZE - 1, 0);
            if (read_len < 0) error("ERROR reading from socket");
            if (read_len == 0) break;

            if (index + read_len <= cipher_len) {
                memcpy(cipher + index, buffer, read_len);
            } else if (index < cipher_len && index + read_len > cipher_len) {
                int split_len = cipher_len - index;
                memcpy(cipher + index, buffer, split_len);
                memcpy(key, buffer + split_len, read_len - split_len);
            } else {
                memcpy(key + index - cipher_len, buffer, read_len);
            }
            index += read_len;

            buffer[read_len] = '\0';
        }
        cipher[cipher_len] = '\0';
        key[key_len] = '\0';

        decrypt(cipher, key, plain, cipher_len);
        plain[cipher_len] = '\0';

        // Send header
        char header[8];
        memcpy(header, "0000", 4);
        int cl = htonl(cipher_len);
        memcpy(header + 4, &cl, 4);
        int written_len = send(conn_fd, header, 8, 0);
        if (written_len < 0) error("SERVER: ERROR writing to socket");

        written_len = send(conn_fd, plain, cipher_len, 0);
        if (written_len < 0) error("SERVER: ERROR writing to socket");

        close(conn_fd); // Close the existing socket which is connected to the client
        sleep(3);
        exit(0);
    }
    close(listen_fd); // Close the listening socket
    return 0;
}
