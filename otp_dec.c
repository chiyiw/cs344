#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#define BUFF_SIZE 256
#define MAX_SIZE 1024 * 1024

void error(const char *msg) { perror(msg); exit(1); }

/**
 * read from file to dest
 * return the number of char successfully readed
 */
int read_file(char *name, char *dest)
{
    char buffer[BUFF_SIZE];
    int read_len = 0;
    FILE *fp = fopen(name, "r");
    if (fp == NULL)
    {
        fprintf(stderr, "Error: can not open file: %s\n", name);
        exit(1);
    }
    int len = 0, i = 0;
    while (fgets(buffer, sizeof(buffer), fp) != NULL)
    {
        // skip the newline
        buffer[strcspn(buffer, "\n")] = '\0';
        if ((len = strlen(buffer)) == 0) break;
        for (i = 0; i < len; i++)
        {
            if ((buffer[i] < 'A' || buffer[i] > 'Z') && buffer[i] != ' ')
            {
                fprintf(stderr, "otp_enc error: input contains bad characters\n");
                exit(1);
            }
        }
        memcpy(dest + read_len, buffer, len);
        read_len += len;
    }
    fclose(fp);
    return read_len;
}

int main(int argc, char *argv[])
{
    char buffer[BUFF_SIZE];
    char plain[MAX_SIZE], key[MAX_SIZE], cipher[MAX_SIZE];
    int plain_len = 0, key_len = 0, cipher_len = 0;

    if (argc < 4)
    {
        fprintf(stderr, "USAGE: %s ciphertext key port\n", argv[0]);
        exit(1);
    }

    // read cipher text from file
    cipher_len = read_file(argv[1], cipher);

    // read key from key file
    key_len = read_file(argv[2], key);

    // compare length of plaintext and key
    if (key_len < cipher_len)
    {
        fprintf(stderr, "Error: key ‘%s’ is too short\n", argv[2]);
        exit(1);
    }

    int socket_fd, port;
    struct sockaddr_in server_addr;
    struct hostent* server_host;

    // Set up the server address struct
    memset((char*)&server_addr, '\0', sizeof(server_addr)); // Clear out the address struct
    port = atoi(argv[3]); // Get the port number, convert to an integer from a string
    server_addr.sin_family = AF_INET; // Create a network-capable socket
    server_addr.sin_port = htons(port); // Store the port number
    server_host = gethostbyname("localhost"); // Convert the machine name into a special form of address
    if (server_host == NULL) { fprintf(stderr, "CLIENT: ERROR, no such host\n"); exit(0); }
    memcpy((char*)&server_addr.sin_addr.s_addr, (char*)server_host->h_addr, server_host->h_length); // Copy in the address

    // Set up the socket
    socket_fd = socket(AF_INET, SOCK_STREAM, 0); // Create the socket
    if (socket_fd < 0) error("CLIENT: ERROR opening socket");

    // Connect to server
    if (connect(socket_fd, (struct sockaddr*)&server_addr, sizeof(server_addr)) < 0)
    {
        fprintf(stderr, "Error: could not contact otp_dec_d on port %s\n", argv[3]);
        exit(2);
    }

    // Send header
    int lc = htonl(cipher_len);
    int lk = htonl(key_len);
    char header[12];
    memcpy(header, "decr", 4);
    memcpy(header + 4, &lc, 4);
    memcpy(header + 8, &lk, 4);
    int written_len = send(socket_fd, &header, 12, 0);
    if (written_len < 0) error("CLIENT: ERROR writing to socket");

    // Send ciphertext
    int send_len = 0;
    while (send_len < cipher_len)
    {
        int written_len = 0;
        if (send_len + BUFF_SIZE - 1 <= cipher_len)
             written_len = send(socket_fd, cipher + send_len, BUFF_SIZE - 1, 0);
        else written_len = send(socket_fd, cipher + send_len, cipher_len - send_len, 0);
        if (written_len < 0) error("CLIENT: ERROR writing to socket");
        send_len += written_len;
    }
    // Send key
    send_len = 0;
    while (send_len < key_len)
    {
        int written_len = send(socket_fd, key + send_len, BUFF_SIZE - 1, 0);
        if (written_len < 0) error("CLIENT: ERROR writing to socket");
        send_len += written_len;
    }

    // Receive header
    int read_len = read(socket_fd, buffer, 8);
    if (read_len < 0) error("CLIENT: ERROR reading from socket");
    char status = buffer[0];
    if (status == '1') {
        fprintf(stderr, "Error: otp_dec cannot use otp_enc_d\n");
        exit(1);
    }

    plain_len = ntohl(*((int*)buffer + 1));
    int index = 0;
    while (index < plain_len)
    {
        read_len = read(socket_fd, buffer, BUFF_SIZE - 1);
        if (read_len < 0) error("CLIENT: ERROR reading from socket");
        memcpy(plain + index, buffer, read_len);
        index += read_len;
    }

    plain[plain_len] = '\0';
    printf("%s\n", plain);

    close(socket_fd);
    return 0;
}
