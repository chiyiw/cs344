all: otp_enc keygen otp_enc_d otp_dec otp_dec_d

otp_enc: otp_enc.c
	gcc otp_enc.c -o otp_enc -g

keygen: keygen.c
	gcc keygen.c -o keygen -g

otp_enc_d: otp_enc_d.c
	gcc otp_enc_d.c -o otp_enc_d -g

otp_dec: otp_dec.c
	gcc otp_dec.c -o otp_dec -g

otp_dec_d: otp_dec_d.c
	gcc otp_dec_d.c -o otp_dec_d -g

clean:
	rm otp_enc keygen otp_enc_d otp_dec otp_dec_d
