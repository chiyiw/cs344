#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX_SIZE 1024 * 1024

int main(int argc, char *argv[])
{
    char key[MAX_SIZE];
    int len = 0;

    if (argc < 2)
    {
        fprintf(stderr, "USAGE: %s keylength\n", argv[0]);
        exit(1);
    }
    len = atoi(argv[1]);
    if (len == 0) {
        fprintf(stderr, "keylength '%s' is invalid", argv[1]);
        exit(1);
    }

    srand(time(NULL));
    int i = 0;
    for (i; i < len; i++) {
        int r = rand() % 27;
        if (r != 26) {
            key[i] = r + 'A';
        } else {
            key[i] = ' ';
        }
    }
    key[len] = '\0';

    printf("%s\n", key);
    return 0;
}
